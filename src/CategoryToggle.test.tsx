import { fireEvent, render, screen } from '@testing-library/react'
import CategoryToggle from './CategoryToggle'
import {
  CATEGORY_ID_GREYHOUND_RACING,
  CATEGORY_ID_HARNESS_RACING,
  CATEGORY_ID_HORSE_RACING,
} from './types'

const mockCall = jest.fn()

beforeEach(() => {
  jest.clearAllMocks()
})

it('calls onChange with the correct parameters', () => {
  render(<CategoryToggle onChange={mockCall} />)

  fireEvent.click(screen.getByTestId('greyhoundRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_GREYHOUND_RACING, true)
  fireEvent.click(screen.getByTestId('greyhoundRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_GREYHOUND_RACING, false)

  fireEvent.click(screen.getByTestId('harnessRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_HARNESS_RACING, true)
  fireEvent.click(screen.getByTestId('harnessRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_HARNESS_RACING, false)

  fireEvent.click(screen.getByTestId('horseRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_HORSE_RACING, true)
  fireEvent.click(screen.getByTestId('horseRacingToggle'))
  expect(mockCall).toBeCalledWith(CATEGORY_ID_HORSE_RACING, false)
})
