import React from 'react'
import './App.css'
import NextToGo from './NextToGo'

function App() {
  return (
    <div className="App">
      <NextToGo />
    </div>
  )
}

export default App
