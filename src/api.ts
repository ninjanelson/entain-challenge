import { RaceSummary } from './types'

interface GetRacesResponse {
  next_to_go_ids: string[]
  race_summaries: RaceSummary
}

export default async function getRaces(
  method: 'nextraces',
  count: number
): Promise<{ status: number; message: string; data: GetRacesResponse }> {
  const searchParams = new URLSearchParams()
  searchParams.append('method', 'nextraces')
  if (count) {
    searchParams.append('count', count.toString())
  }

  const response = await fetch(
    `https://api.neds.com.au/rest/v1/racing/?${searchParams}`
  )
  return response.json()
}
