import React from 'react'
import { Race } from './types'
import _ from 'lodash'
import moment from 'moment'
import Countdown from './Countdown'

/** Displays a table of races, with meeting name, race number and a countdown timer for the starting time.
 *
 * @param races - The list of races to display
 * @param categories - The category ids enabled to be shown, all categories are displayed if empty
 * @param onExpire - Function to call when a race started more than one minute ago
 */
export default function RaceTable({
  races,
  categories,
  onExpire,
}: {
  races: [string, Race][]
  categories: string[]
  onExpire: (key: string) => void
}) {
  return (
    <table>
      <tbody>
        <tr>
          <th>Meeting Name</th>
          <th>Race Number</th>
          <th>Starts in</th>
        </tr>
        {races
          .filter(([, value]) =>
            // Only include selected categories, if none are selected, then display all
            categories.length === 0
              ? true
              : _.includes(categories, value.category_id)
          )
          .filter(([, value]) => {
            // Ensure that the race did not start more than one minute ago
            const timeThreshold = moment
              .unix(value.advertised_start.seconds)
              .add(1, 'minute')
            return moment().isBefore(timeThreshold)
          })
          .sort(
            // Sort by start time ascending
            ([, a], [, b]) =>
              a.advertised_start.seconds - b.advertised_start.seconds
          )
          .slice(0, 5)
          .map(([key, value]) => {
            return (
              <tr key={key}>
                <td>{value.meeting_name}</td>
                <td>{value.race_number}</td>
                <td>
                  <Countdown
                    time={value.advertised_start.seconds}
                    onExpire={() => onExpire(key)}
                  />
                </td>
              </tr>
            )
          })}
      </tbody>
    </table>
  )
}
