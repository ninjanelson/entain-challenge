import { act, render, waitFor } from '@testing-library/react'
import NextToGo from './NextToGo'

it('should render', () => {
  render(<NextToGo />)
})

it.only('should call api on load', async () => {
  jest.spyOn(global, 'fetch').mockImplementation(
    jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({ data: { race_summaries: { test: {} } } }),
      })
    ) as jest.Mock
  )
  render(<NextToGo />)
  await waitFor(() => {
    expect(fetch).toBeCalledWith(
      'https://api.neds.com.au/rest/v1/racing/?method=nextraces&count=50'
    )
  })
})
