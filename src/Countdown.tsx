import React, { useEffect, useRef, useState } from 'react'
import moment from 'moment'

/** Displays a countdown timer with hours, minutes, and seconds reamining, where applicable
 *
 * @param time - Unix timestamp in milliseconds
 * @param onExpire - Function to call when the time expired more than one minute ago
 */
export default function Countdown({
  time,
  onExpire,
}: {
  time: number
  onExpire: () => void
}) {
  const [seconds, setSeconds] = useState<number>()
  const [minutes, setMinutes] = useState<number>()
  const [hours, setHours] = useState<number>()
  const timer = useRef<any>()

  // Tick function, runs every second
  useEffect(() => {
    timer.current = setInterval(() => {
      const endTime = moment.unix(time)
      const duration = moment.duration(endTime.diff(moment()))
      const secondsRemaining = duration.seconds()
      const minutesRemaining = duration.minutes()
      const hoursRemaining = duration.hours()
      setSeconds(secondsRemaining)
      setMinutes(minutesRemaining)
      setHours(hoursRemaining)
      // Sets an item as expired when the start time is more than one minute ago
      if (minutesRemaining <= -1) {
        onExpire()
      }
    }, 1000)
    return () => clearInterval(timer.current)
  }, [time, onExpire])

  if (hours === undefined && minutes === undefined && seconds === undefined) {
    return <div />
  } else if (hours && hours > 0) {
    return <div>{`${hours}h ${minutes}m ${seconds}s`}</div>
  } else if (minutes && minutes >= 0) {
    return <div>{`${minutes}m ${seconds}s`}</div>
  } else {
    return <div>{`${seconds}s`}</div>
  }
}
