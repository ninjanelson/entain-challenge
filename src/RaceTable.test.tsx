import { render, screen } from '@testing-library/react'
import RaceTable from './RaceTable'
import {
  CATEGORY_ID_GREYHOUND_RACING,
  CATEGORY_ID_HARNESS_RACING,
  CATEGORY_ID_HORSE_RACING,
  RaceSummary,
} from './types'
import moment from 'moment'

const ALL_CATEGORIES = [
  CATEGORY_ID_HORSE_RACING,
  CATEGORY_ID_HARNESS_RACING,
  CATEGORY_ID_GREYHOUND_RACING,
]

const ALL_TEST_RACES = {
  greyhound: {
    advertised_start: { seconds: moment.now() },
    meeting_name: 'Test Greyhound Race',
    race_number: 1,
    category_id: CATEGORY_ID_GREYHOUND_RACING,
  },
  harness: {
    advertised_start: { seconds: moment.now() },
    meeting_name: 'Test Harness Race',
    race_number: 1,
    category_id: CATEGORY_ID_HARNESS_RACING,
  },
  horse: {
    advertised_start: { seconds: moment.now() },
    meeting_name: 'Test Horse Race',
    race_number: 1,
    category_id: CATEGORY_ID_HORSE_RACING,
  },
} as unknown as RaceSummary
const mockExpire = jest.fn()

it('renders all race types with no categories selected', () => {
  render(
    <RaceTable
      races={Object.entries(ALL_TEST_RACES)}
      categories={[]}
      onExpire={mockExpire}
    />
  )
  expect(screen.getByText('Test Greyhound Race')).toBeInTheDocument()
  expect(screen.getByText('Test Harness Race')).toBeInTheDocument()
  expect(screen.getByText('Test Horse Race')).toBeInTheDocument()
})

it('renders all race types with all categories selected', () => {
  render(
    <RaceTable
      races={Object.entries(ALL_TEST_RACES)}
      categories={ALL_CATEGORIES}
      onExpire={mockExpire}
    />
  )
  expect(screen.getByText('Test Greyhound Race')).toBeInTheDocument()
  expect(screen.getByText('Test Harness Race')).toBeInTheDocument()
  expect(screen.getByText('Test Horse Race')).toBeInTheDocument()
})

it('only renders greyhound races', () => {
  render(
    <RaceTable
      races={Object.entries(ALL_TEST_RACES)}
      categories={[CATEGORY_ID_GREYHOUND_RACING]}
      onExpire={mockExpire}
    />
  )
  expect(screen.getByText('Test Greyhound Race')).toBeInTheDocument()
  expect(screen.queryByText('Test Harness Race')).toBeFalsy()
  expect(screen.queryByText('Test Horse Race')).toBeFalsy()
})

it('only renders harness races', () => {
  render(
    <RaceTable
      races={Object.entries(ALL_TEST_RACES)}
      categories={[CATEGORY_ID_HARNESS_RACING]}
      onExpire={mockExpire}
    />
  )
  expect(screen.queryByText('Test Greyhound Race')).toBeFalsy()
  expect(screen.getByText('Test Harness Race')).toBeInTheDocument()
  expect(screen.queryByText('Test Horse Race')).toBeFalsy()
})

it('only renders horse races', () => {
  render(
    <RaceTable
      races={Object.entries(ALL_TEST_RACES)}
      categories={[CATEGORY_ID_HORSE_RACING]}
      onExpire={mockExpire}
    />
  )
  expect(screen.queryByText('Test Greyhound Race')).toBeFalsy()
  expect(screen.queryByText('Test Harness Race')).toBeFalsy()
  expect(screen.getByText('Test Horse Race')).toBeInTheDocument()
})

it('does not render races that started more than one minute ago', () => {
  const expired = {
    expired: {
      advertised_start: {
        seconds: moment.unix(moment.now()).add('1', 'minute'),
      },
      meeting_name: 'Test Greyhound Race',
      race_number: 1,
      category_id: CATEGORY_ID_GREYHOUND_RACING,
    },
  } as unknown as RaceSummary

  render(
    <RaceTable
      races={Object.entries(expired)}
      categories={[CATEGORY_ID_GREYHOUND_RACING]}
      onExpire={mockExpire}
    />
  )

  expect(screen.queryByText('Test Greyhound Race')).toBeFalsy()
})
