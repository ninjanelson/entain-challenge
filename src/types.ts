export interface RaceSummary {
  [key: string]: Race
}

export interface Race {
  advertised_start: { seconds: number }
  category_id: string
  meeting_id: string
  meeting_name: string
  race_form: any
  race_id: string
  race_name: string
  race_number: number
  venue_country: string
  venue_id: string
  venue_name: string
  venue_state: string
}

export const CATEGORY_ID_GREYHOUND_RACING =
  '9daef0d7-bf3c-4f50-921d-8e818c60fe61'
export const CATEGORY_ID_HARNESS_RACING = '161d9be2-e909-4326-8c2c-35ed71fb460b'
export const CATEGORY_ID_HORSE_RACING = '4a2788f8-e825-4d36-9894-efd4baf1cfae'
