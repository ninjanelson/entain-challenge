import React from 'react'
import {
  CATEGORY_ID_GREYHOUND_RACING,
  CATEGORY_ID_HARNESS_RACING,
  CATEGORY_ID_HORSE_RACING,
} from './types'

export default function CategoryToggle({
  onChange,
}: {
  onChange: (category_id: string, checked: boolean) => void
}) {
  return (
    <form>
      <input
        data-testid={'greyhoundRacingToggle'}
        name={'greyhoundRacingToggle'}
        type={'checkbox'}
        onChange={(event) =>
          onChange(CATEGORY_ID_GREYHOUND_RACING, event.target.checked)
        }
      />
      <label>Greyhound racing</label>

      <input
        data-testid={'harnessRacingToggle'}
        name={'harnessRacingToggle'}
        type={'checkbox'}
        onChange={(event) =>
          onChange(CATEGORY_ID_HARNESS_RACING, event.target.checked)
        }
      />
      <label>Harness racing</label>

      <input
        data-testid={'horseRacingToggle'}
        name={'horseRacingToggle'}
        type={'checkbox'}
        onChange={(event) =>
          onChange(CATEGORY_ID_HORSE_RACING, event.target.checked)
        }
      />
      <label>Horse racing</label>
    </form>
  )
}
