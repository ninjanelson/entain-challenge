import React, { useEffect, useState } from 'react'
import { RaceSummary } from './types'
import getRaces from './api'
import _ from 'lodash'
import CategoryToggle from './CategoryToggle'
import RaceTable from './RaceTable'

/** Displays a table of Next to Go races, with filterable categories. Any race which has started more than one
 * minute ago will not be displayed.
 */
export default function NextToGo() {
  const [races, setRaces] = useState<RaceSummary>({})
  const [categories, setCategories] = useState<string[]>([])

  async function fetchAndSetRaces() {
    return getRaces('nextraces', 50).then((res) => {
      setRaces(res.data.race_summaries)
      return res
    })
  }

  useEffect(() => {
    ;(async function () {
      await fetchAndSetRaces()
    })()
  }, [])

  useEffect(() => {
    ;(async function () {
      const displayedRacesCount = Object.entries(races).filter(([, value]) =>
        // Only include selected categories, if none are selected, then display all
        categories.length === 0
          ? true
          : _.includes(categories, value.category_id)
      ).length
      if (displayedRacesCount < 5) {
        await fetchAndSetRaces()
      }
    })()
  }, [races, categories])

  function onFilterChange(category_id: string, checked: boolean) {
    if (checked) {
      setCategories((prevState) => [...prevState, category_id])
    } else {
      setCategories((prevState) => _.without(prevState, category_id))
    }
  }

  function onExpire(key: string) {
    const newSummary = _.omit(races, key)
    setRaces(newSummary)
  }

  return (
    <div>
      <h2>Next To Go</h2>

      <CategoryToggle onChange={onFilterChange} />
      <br />

      <RaceTable
        races={Object.entries(races)}
        categories={categories}
        onExpire={onExpire}
      />
    </div>
  )
}
