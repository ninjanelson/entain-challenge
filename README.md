# Entain Technical Task

The goal of this task is to create a single page application that displays 'Next to go' races using the Neds api.

The application has been written in React + Typescript.

## Running the app

```
yarn start
```

## Running tests

```
yarn test
```

Created by Nelson Gregory
